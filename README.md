# Emoji Search

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

### Install

```
npm install
```

### Usage

```
npm start
```


## Docker Usage

Building the image:

```
docker build -f dockerfiles/Dockerfile-app -t diofeher/diogenes-21-02-2021 .
```

Running the image:

```
docker run -p 8081:8080 diofeher/diogenes-21-02-2021
```

Access application at http://localhost:8081/

## Kubernetes Usage

If you are using Docker Desktop, you just need to

```
export TITLE="React App Example Title"
docker build -t diofeher/diogenes-21-02-2021 . --build-arg TITLE --tag diofeher/diogenes-21-02-2021:local
kubectl apply -f deployment/app.yaml
```

Access application at http://localhost:8081/

## Tilt Usage

To use with [Tilt.Dev](https://tilt.dev/), just download, install and then run:

```
tilt up
```

Access application at http://localhost:8081/

# Important points

- Use a bash script to change source code is considered a bad practice. I'm using an environment variable instead to change the title.
- Not using StatefulSet so it's easier to scale and a React app doesn't need to hold state.
- I've used `extends` from Gitlab in favor of `YAML anchors` as it provides better readability.
- Using browsable artifacts to hold Review apps.

# Addressed security issues

- Running nginx as normal user;
- No passwords are added to CI/CD pipelines, ephemeral CI_JOB_TOKENs are used instead;

# Further improvements

- Due to lack of clarity on deployment, I decided to go with normal `npm run build` and then passing the bundle as artifact to deploy stage + integration with Gitlab pages.
- Create deployment using Docker builds, not using `npm run build` on Gitlab Jobs, and then Kubernetes deploy.
- The current way the Review Apps is done is not good, I'm duplicating the artifacts and didn't setup in a better way, unfortunately due to lack of time I decided to leave this way.
- Improve caching of node_modules to be used only when necessary.
- Hardening of containers and kubernetes
- Integrate security CI/CD pipelines (image scanning, secrets detection)
- Use namespaces to isolate workloads
- Use Kubernetes network policies to control traffic between clusters
- Monitor network traffic and deployment resources
